#!/bin/bash
set -euo errexit

printf "\n[-] Building Meteor application bundle...\n\n"

mkdir -p /bundle/

# for profiling, add this prefix:
# - env VELOCITY_DEBUG=1 VELOCITY_DEBUG_MIRROR=1 METEOR_PROFILE=1
# for bigger memory:
# - env NODE_OPTIONS="--max-old-space-size=2048" TOOL_NODE_FLAGS="--max-old-space-size=4096" 
meteor build --directory /tmp --server-only
mv /tmp/bundle/* /bundle/

