#!/bin/bash
set -euo errexit

printf "\n[-] Installing app Yarn dependencies [args: %s]...\n\n" "$@"

meteor npx yarn --frozen-lockfile --network-timeout 100000 "$@" install
meteor npx yarn cache clean
