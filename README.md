# meteor-docker

Optimized docker & CI for Meteor framework

## Getting started


1. Copy this into your app repo
2. Check `Dockerfile` if the paths & tools match your setup
3. Adapt `.dockerignore` to ignore everything not needed for building the app