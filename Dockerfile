##############################
# Meteor & prod dependencies #
##############################
# The tag here should match the Meteor version of your app, per .meteor/release
FROM geoffreybooth/meteor-base:2.7.2 as base-prod
RUN mkdir -p /app
WORKDIR /app

# Scripts modified from https://github.com/disney/meteor-base/tree/main/src/docker to:
# - use Yarn instead of NPM
COPY ./docker-scripts/* /docker/
# Copy app yarn files into container (so that the yarn step can be cached independently of app files)
COPY ./package.json ./yarn.lock ./
RUN bash /docker/build-app-yarn-dependencies.sh --prod=true

#########################
# Yarn dev dependencies # (this stage is only used for e.g. eslint stage in CI)
#########################
FROM base-prod as base-dev
RUN bash /docker/build-app-yarn-dependencies.sh --prod=false

###################
# Meteor packages #
###################
FROM base-prod as meteor-base
# Copy .meteor files (including local/*-cache from CI cache) into container
COPY .meteor/ .meteor/
# Copy custom packages (otherwise meteor list will fail because of unknown package)
COPY packages/ packages/
# this might look confusing.... and it is... but this command is the only way I found to just install packages - without building or running the full app
RUN meteor list

#############
# App Bundle #
#############
FROM base-prod as app-files
# Copy app source into container (be sure to have a comprehensive .dockerignore)
COPY ./ ./
# Add meteor caches (TODO: check if this is actually useful for this stage)
COPY --from=meteor-base /app/.meteor/local /.meteor/local
# Add meteor packages (yeah, meteor likes to put stuff in the home folder, I know, I love it too :/)
COPY --from=meteor-base /root/.meteor /root/.meteor
RUN bash /docker/build-meteor-bundle.sh


##########
# RUNNER #
##########
# You should use the version of Node expected by your Meteor release, per https://docs.meteor.com/changelog.html
FROM node:14-alpine as runner

# Add needed dependencies
RUN apk add --no-cache bash
# helpful tools for debugging/ops:
#RUN apk add --no-cache bash ca-certificates mongodb-tools

# Copy in scripts
COPY --from=base-prod /docker/ /docker/
# Copy in app bundle
COPY --from=app-bundle /bundle/ /bundle/

# Rebuild some dependencies that need it
#TODO: is this needed here and can't be up in e.g. bundle stage?
# --virtual => Install OS build dependencies, which we remove directly after we’ve compiled native Node extensions
RUN ls /bundle \
	&& apk --no-cache --virtual .build-deps add build-base python3 \
	&& bash /docker/build-meteor-npm-dependencies.sh \
	&& cd /bundle/programs/server/npm \
	&& npm rebuild --build-from-source bcrypt \
	&& apk del .build-deps

# Start app
ENTRYPOINT ["/docker/entrypoint.sh"]
CMD ["node", "main.js"]
